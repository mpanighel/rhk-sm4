# rhk_sm4

Important note
==============

This package is no longer maintained and has been superseded by [spym](https://github.com/rescipy-project/spym).

Use the new `spym` package with the following to get the same interface of `rhksm4`:

```
from spym.io import rhksm4
```
